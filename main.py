__author__ = 'japurane'

import re
import requests
from bs4 import BeautifulSoup


startPage = 'http://www.capitalheels.fi/tanssit.htm'
yearsIndex = ["2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013"]

years = []

r = requests.get(startPage)
soup = BeautifulSoup(r.text)
links = soup.find_all("a", href=re.compile("tanssit"))

for year in yearsIndex:
  yearData =  filter( lambda link: re.search(year,link.text), links)
  years.append((year,yearData))




